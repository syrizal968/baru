@extends('layouts/app')
@section('content')

<div class="container">
                    <div class="row">

                        <div class="col-4">

                            <!-- Default Card Example -->
                            <div class="card mb-4">
    <div class="card">
      <img src="{{asset('assets/img/mm.jpg')}}" class="card-img-top" alt="" style="height:250px">
      <div class="card-body">
        <h5 class="card-title">{{ auth()->guard('siswa')->user()->name }}</h5>
      
        <div  class="d-sm-flex align-items-center justify-content-between mt-4">
        <a href="/cetak/pdf" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i
        class="fas fa-download fa-sm text-white-50"></i> Cetak Pdf</a>
        <a href="/edit/profile" class="d-none d-sm-inline-block btn btn-sm btn-success shadow-sm"><i 
            class="fas fa-edit float-right fa-sm text-white-50"></i>Edit </a>
        
        </div>
      </div>
    </div>

                            </div>

                            <!-- Basic Card Example -->


                        </div>

                        <div class="col-8">

                            <!-- Dropdown Card Example -->
                            <div class="card shadow mb-4">
                                <!-- Card Header - Dropdown -->
                                    <!-- [ tabs ] start -->
                                            <div class="">
                                                <ul class="nav nav-tabs mb-3" id="myTab" role="tablist">
                                                    <li class="nav-item">
                                                        <a class="nav-link active text-uppercase" id="home-tab" data-toggle="tab"
                                                            href="#semua-pendaftaran" role="tab" aria-controls="home" aria-selected="true">Karya</a>
                                                    </li>
                                                    {{-- <li class="nav-item">
                                                        <a class="nav-link text-uppercase" id="profile-tab" data-toggle="tab"
                                                            href="#sedang-mendaftar" role="tab" aria-controls="profile"
                                                            aria-selected="false">Keahlian</a>
                                                    </li>
                                                    <li class="nav-item">
                                                        <a class="nav-link text-uppercase" id="contact-tab" data-toggle="tab" href="#pengumuman"
                                                            role="tab" aria-controls="contact" aria-selected="false">Pengalaman</a>
                                                    </li> --}}
                                                    <li class="nav-item">
                                                        <a class="nav-link text-uppercase" id="contact-tab" data-toggle="tab" href="#dibatalkan"
                                                            role="tab" aria-controls="contact" aria-selected="false">Pencapaian</a>
                                                    </li>
                                                </ul>
                                            </div>
                                            <div class="tab-content mb-5" id="myTabContent">
                                                <div class="tab-pane fade show active" id="semua-pendaftaran" role="tabpanel"
                                                    aria-labelledby="home-tab">
                                                    <div class="card-body">
                                                        <form method="POST" action="/tambahkarya" enctype="multipart/form-data">
                                                            @csrf
                                                            <div class="form-group">
                                                                <input type="file" class="form-control form-control-user"
                                                                    id="exampleInputEmail" aria-describedby="emailHelp"
                                                                    placeholder="Couse Foto" class="fas fa-file fa-sm text-white-50" name="file">
                                                            </div>

                                                            <div class="form-group">
                                                                <select class="form-control" name="kategori_id" id="exampleFormControlSelect1" required>
                                                                    <option value="">Pilih Kategori</option>
                                                                    @foreach ($kategori as $item)
                                                                    <option value="{{ $item->id }}">{{ $item->kategori}}</option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                            <div class="form-group">
                                                                <select class="form-control" name="kelas_id" id="exampleFormControlSelect1" required>
                                                                    <option value="">Pilih kelas</option>
                                                                    @foreach ($kelas as $item)
                                                                    <option value="{{ $item->id }}">{{ $item->kelas }}</option>
                                                                    @endforeach
                                                                </select>
                                                            </div>


                                                            <div class="form-group">
                                                                <input type="text" class="form-control form-control-user"
                                                                    id="exampleInputPassword" placeholder="Deskripsi" name="deskripsi">
                                                            </div>
                                                                <button type="submit" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i
                                                            class="fas fa-file fa-sm text-white-100"></i> Submit</button>
                                                        </form>
                                                    </div>
                                                </div>
                                                {{-- <div class="tab-pane fade" id="sedang-mendaftar" role="tabpanel" aria-labelledby="profile-tab">
                                                    <div class="card-body">
                                                        <form method="POST" action="" enctype="multipart/form-data">
                                                            @csrf
                                                            <div class="form-group">
                                                                <input type="text" class="form-control form-control-user"
                                                                    id="exampleInputEmail" aria-describedby="emailHelp"
                                                                    placeholder="Keahlian" name="Keahlian">
                                                            </div>
                                                                <button type="submit" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i
                                                            class="fas fa-file fa-sm text-white-100"></i> Submit</button>
                                                        </form>
                                                    </div>
                                                </div> --}}
                                                {{-- <div class="tab-pane fade" id="pengumuman" role="tabpanel" aria-labelledby="contact-tab">
                                                    <div class="card-body">
                                                        <form method="POST" action="" enctype="multipart/form-data">
                                                            @csrf
                                                            <div class="form-group">
                                                                <input type="text" class="form-control form-control-user"
                                                                    id="exampleInputEmail" aria-describedby="emailHelp"
                                                                    placeholder="Pengalaman" name="pengalaman">
                                                            </div>
                                                                <button type="submit" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i
                                                            class="fas fa-file fa-sm text-white-100"></i> Submit</button>
                                                        </form>
                                                    </div>
                                                </div> --}}
                                                <div class="tab-pane fade" id="dibatalkan" role="tabpanel" aria-labelledby="contact-tab">
                                                    <div class="card-body">
                                                        <form method="POST" action="" enctype="multipart/form-data">
                                                            @csrf
                                                            <div class="form-group">
                                                                <input type="text" class="form-control form-control-user"
                                                                    id="exampleInputEmail" aria-describedby="emailHelp"
                                                                    placeholder="Pencapaian" name="pencapaian">
                                                            </div>
                                                                <button type="submit" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i
                                                            class="fas fa-file fa-sm text-white-100"></i> Submit</button>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                    <!-- [ tabs ] end -->

                            </div>

                            <!-- Collapsable Card Example -->

                        </div>

                    </div>
</div>


@endsection
