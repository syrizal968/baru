@extends('layouts/app')
@section('content')
{{-- @guest
    @php --}}
        // Redirect to login if user ID is null
        {{-- return redirect()->route('login'); --}}
    {{-- @endphp
@endguest --}}

<div class="container">
        <div class="card mb-3 p-5">
        <div class="mt-4 ">
            <img src="{{asset('assets/img/mm.jpg')}}" style="height: 140px; width: 140px;" class="rounded mx-auto d-block rounded-circle" alt="Foto Profil Anda">
        </div>
        <div class="mt-3">
            <h4 class="text-center">Hallo👋 {{ auth()->guard('siswa')->user()->name }}</h4>
            <p class="text-center" style="font-size: 16px">Halo, saya [ {{ auth()->guard('siswa')->user()->name }} ], {{ auth()->guard('siswa')->user()->keterangan }}</p>
            <p class="text-center">*----------------*</p>
        </div>
    </div>
<div class="btn-group">
    <div class="btn-group p-2">
        <buttlion class="btn btn-primary dropdown-toggle" type="button"
            id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true"
            aria-expanded="false">
            Kategori
        </buttlion>
        <div class="dropdown-menu animated--fade-in"
            aria-labelledby="dropdownMenuButton">
            @foreach ($kategori as $item)
                <a class="dropdown-item" href="/siswa/kategori/{{$item->id}}">{{$item->kategori}}</a>
            @endforeach
        </div>
    </div>
    <div class="dropdown p-2">
        <button class="btn btn-primary dropdown-toggle" type="button"
            id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true"
            aria-expanded="false">
            Kelas
        </button>
        <div class="dropdown-menu animated--fade-in"
            aria-labelledby="dropdownMenuButton">
            <a class="dropdown-item" href="#">Class X</a>
            <a class="dropdown-item" href="#">Class XI</a>
            <a class="dropdown-item" href="#">Class XII</a>
        </div>
    </div>
</div>
    <div class="card mt-3 mb-5">
        <div class="container">

    <div class="row row-cols-1 row-cols-md-3 g-4 mt-4 mb-4">
    @foreach ($karya as $item)
    <div class="col mb-4" >
        <div class="card h-100">
            <div class="">
            <img src="{{ asset('filekarya/'.$item->file) }}"  alt="karya" class="card-img-top " style=" height: 250px;" >
            </div>
            <div class="card-body">
                <h5 class="card-title">{{$item->kategori->kategori }}</h5>
                <p class="card-text">{{$item->deskripsi}}</p>
                <p class="card-text">{{$item->kelas->kelas}}</p>

            </div>
            {{-- <div class="">
                <a data-toggle="modal" data-target="#exampleModal" class="btn btn-primary ml-3 mb-2">Edit</a>
                <a href="/hapuskarya/{{ $item->id }}" class="btn btn-danger mb-2">Hapus</a>
            </div> --}}
        </div>

    </div>
    @endforeach
    </div>
</div>
</div>
</div>

  <!-- Modal -->
  {{-- <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Edit Karya</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <form method="POST" action="/tambahkarya" enctype="multipart/form-data">
            @csrf
            <div class="form-group">
                <input type="file" class="form-control form-control-user"
                    id="exampleInputEmail" aria-describedby="emailHelp"
                    placeholder="Couse Foto" class="fas fa-file fa-sm text-white-50" name="file">
            </div>

            <div class="form-group">
                <input type="text" class="form-control form-control-user"
                    id="exampleInputEmail" aria-describedby="emailHelp"
                    placeholder="Kategori" name="kategori">
            </div>

            <div class="form-group">
                <input type="text" class="form-control form-control-user"
                    id="exampleInputPassword" placeholder="Deskripsi" name="deskripsi">
            </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          <button type="button" class="btn btn-primary">Simpan</button>
        </div>
    </form>
      </div>
    </div>
  </div> --}}

@endsection
