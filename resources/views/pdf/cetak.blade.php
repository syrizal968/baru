@extends('layouts/app')
@section('content')
    <div class="col-xl-12">
        <div class="card card-body" id="cetak" style="margin-bottom: -1rem">
            <div class="p-4">
                <div class="d-flex">
                    <div class="col-lg-3" style="text-align: center; margin-right:25px; margin-left:25px;">
                        <img width="110px" src="{{asset('assets/img/LOGO.webp')}}" alt="">
                    </div>
                    <div class="col-lg-6">
                        <center>
                            <label class="form-label" style="margin-top: -0.5rem"><strong class="d-block">PORTOFOLIO SISWA
                                    </strong></label>
                            <h5 style="margin-top: -0.5rem"> <strong class="d-block">UNIVERSITAS ISLAM MADURA</strong></h4>
                                {{-- <h4 style="margin-top: -0.5rem"><strong class="d-block">POLITEKNIK ENJINERING INDORAMA</strong></h3> --}}
                                    <p style="margin-top: -0.5rem"><strong class="d-block">JL. Pondok Peantren Miftahul Ulum Bettet, Pamekasan Madura, Gladak, Bettet, Kec. Pamekasan, Kabupaten Pamekasan, Jawa Timur <br> 69317</strong></p>
                        </center>
                    </div>
                </div>
            </div>
            <div class="card-body" style="margin-bottom: -4rem;">
                <div class="p-4" style="border-top: 2px solid black!important; margin-top:-2.5rem;">
                    <div class="mb-4">
                        <h5> Nama : {{ auth()->guard('siswa')->user()->name }}</h5>
                        <h6 style="text-indent: 0.5in;text-align: justify;">{{ auth()->guard('siswa')->user()->tempat_lahir }}</h6>
                        <br>
                        <h4><strong>Keterangan</strong></h4>
                        <h5 style="text-indent: 0.5in;text-align: justify;">{{ auth()->guard('siswa')->user()->keterangan }}</h5>
                    </div>
                    <div class="row">
                    @foreach ($karya as $item)
                    <div class="container book-box mb-2">
                        <div class="row row-one">
                            <div class="col-md-">
                                <img src="{{ asset('filekarya/'.$item->file) }}"  alt="karya" class="card-img-top " style=" height: 100px; width: 100px;" >
                            </div>
                            <div class="col-md-">
                                <h6 class="card-title">{{$item->kategori->kategori }}</h6>
                                <p class="card-text">{{$item->deskripsi}}</p>
                                <p class="card-text">{{$item->kelas->kelas}}</p>
                            </div>  
                        </div>
                    </div>
                    @endforeach 
                </div>
            </div>
        </div>
    </div>

    <div class="row my-4">
        <div class="col">
            <div class="text-end mt-2 mt-sm-0">
                <a href="/export/pdf" class="btn btn-success waves-effect waves-light me-1"><i
                        class="fa fa-print"></i></a>
            </div>
        </div>
        <!-- end col -->
    </div>
@endsection 