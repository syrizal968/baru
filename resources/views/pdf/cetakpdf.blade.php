<!DOCTYPE html>
<html lang="id">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Portofolio Siswa</title>
    <style>
        body {
            font-family: Arial, sans-serif;
        }
        .header {
            background-color: #ffffff;
            color: #000000;
            padding: 10px;
            text-align: center;
        }
        .content {
            margin: 20px;
        }
        .footer {
            background-color: #f1f1f1;
            padding: 10px;
            text-align: center;
        }
    </style>
</head>
<body>
    <div class="header">
        <h1>PORTOFOLIO SISWA</h1>
        <p>UNIVERSITAS ISLAM MADURA</p>
        <p>JL. Pondok Peantren Miftahul Ulum Betet, Pamekasan Madura, Gladak, Betet, Kec. Pamekasan, Kabupaten Pamekasan, Jawa Timur 69317</p>
    </div>
    <hr>

    <div class="content">
        <h2>Nama: {{ auth()->guard('siswa')->user()->name }}</h2>
        <p>{{ auth()->guard('siswa')->user()->keterangan }}</p>
    </div>
    <div class="content">
    @foreach ($karya as $item)
                <h6 class="card-title">Kategori : {{$item->kategori->kategori }}, Kelas : {{$item->kelas->kelas}}, Deskripsi : {{$item->deskripsi}} </h6>
    @endforeach 
</div>
<div>
    <a href="/export/pdf">cetak</a>
</div>
</body>
</html>