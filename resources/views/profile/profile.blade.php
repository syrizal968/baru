@extends('layouts/app')
@section('content')
<div class="row mb-4">
    <div class="col-xl-4">
        <div class="card">
            <div class="card-body">
                <div class="text-center">
                    <div class="dropdown float-end">
                        {{-- <a class="text-body dropdown-toggle font-size-18" href="#" role="button"
                            data-bs-toggle="dropdown" aria-haspopup="true"> <i class="uil uil-ellipsis-v"></i> </a> --}}
                        <div class="dropdown-menu dropdown-menu-end"> <a class="dropdown-item"
                                href="profile">Perbaharui Data</a> <a class="dropdown-item"
                                href="/profile">Segarkan</a>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div>    
                                {{-- @if ({{ auth()->guard('siswa')->user()->foto }} != null) --}}
                                {{-- <img class="avatar-lg rounded-circle img-thumbnail"
                                src="{{ url('/' . auth()->guard('siswa')->user()->foto) }}" alt="" width="100px" height="100px" />
                                @else --}}
                                    <img class="img-profile rounded-circle"
                                        src="{{asset('assets/img/mm.jpg')}}" alt=""
                                        width="100px" height="100px" />
                                
                                {{-- @endif --}}
                    </div>
                    <h5 class="mt-3 mb-1">
                    </h5>
                </div>
                <hr class="my-4">
                <div class="text-muted">
                    <div class="table-responsive mt-4">
                        @auth

                            <div>
                                <p class="mb-1">Nama : {{ auth()->guard('siswa')->user()->name }}</p>
                                <h5 class="font-size-16">
                                </h5>
                            </div>
                            <div class="mt-4">
                                <p class="mb-1">Nis : {{ auth()->guard('siswa')->user()->username }}</p>
                                <h5 class="font-size-16">
                                </h5>
                            </div>
                        @endauth
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-xl-8">
        <div class="card">
            <div class="card-body">
                <div class="profile-tab">
                    <div class="custom-tab-1">
                        <ul class="nav nav-tabs">
                            <li class="nav-item"><a href="#about-me" data-bs-toggle="tab"
                                    class="nav-link active show">Profil</a>
                            </li>
                        </ul>
                        <div class="tab-content">
                            <div id="about-me" class="tab-pane fade active show">
                                <a class="btn btn-info mt-4" href="/edit/profile">Edit</a>
                                <div class="profile-personal-info mt-4">
                                            <h4 class="text-primary mb-4">Informasi Tambahan</h4>
                                            <div class="row mb-2">
                                                <div class="col-sm-3 col-5">
                                                    <h5 class="f-w-500">Nama </h5>
                                                </div>
                                                {{-- <div class="col-sm-9 col-7">{{ auth()->user()->profile->nama }} --}}
                                                <div class="col-sm-9 col-7">: {{ auth()->guard('siswa')->user()->name }}
                                                </div>
                                            </div>
                                            <div class="row mb-2">
                                                <div class="col-sm-3 col-5">
                                                    <h5 class="f-w-500">Jenis Kelamin </h5>
                                                </div>
                                                <div class="col-sm-9 col-7">: {{ auth()->guard('siswa')->user()->jenis_kelamin }}
                                                </div>
                                            </div>
                                            <div class="row mb-2">
                                                <div class="col-sm-3 col-5">
                                                    <h5 class="f-w-500">Tempat Lahir</h5>
                                                </div>
                                                <div class="col-sm-9 col-7">: {{ auth()->guard('siswa')->user()->tempat_lahir }}
                                                </div>
                                            </div>
                                            <div class="row mb-2">
                                                <div class="col-sm-3 col-5">
                                                    <h5 class="f-w-500">Tanggal Lahir</h5>
                                                </div>
                                                <div class="col-sm-9 col-7">: {{ auth()->guard('siswa')->user()->tanggal_lahir }}
                                                </div>
                                            </div>
                                            {{-- <div class="row mb-2">
                                                <div class="col-sm-3 col-5">
                                                    <h5 class="f-w-500">Alamat
                                                    </h5>
                                                </div>
                                                <div class="col-sm-9 col-7">
                                                </div>
                                            </div> --}}
                                            <br>
                                            <h4 class="text-primary mb-4">Informasi keterangan</h4>
                                            <div class="row mb-2">
                                                <div class="col-sm-3 col-5">
                                                    <h5 class="f-w-500">Keterangan</h5>
                                                </div>
                                                <div class="col-sm-9 col-7">: {{ auth()->guard('siswa')->user()->keterangan }}
                                                </div>
                                            </div>
                                </div> 
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- end row -->
@endsection 