@extends('layouts/app')
@section('content')
<div class="row mb-4">
    <div class="col-xl-4">
        <div class="card">
            <div class="card-body">
                <div class="text-center">
                    <div class="dropdown float-end">
                        {{-- <a class="text-body dropdown-toggle font-size-18" href="#" role="button"
                            data-bs-toggle="dropdown" aria-haspopup="true"> <i class="uil uil-ellipsis-v"></i> </a> --}}
                        {{-- <div class="dropdown-menu dropdown-menu-end"> <a class="dropdown-item"
                                href="profile">Perbaharui Data</a> <a class="dropdown-item"
                                href="/profile">Segarkan</a>
                        </div> --}}
                    </div>
                    <div class="clearfix"></div>
                    <div>    
                                {{-- @if ({{ auth()->guard('siswa')->user()->foto }} != null) --}}
                                <img class="avatar-lg rounded-circle img-thumbnail"
                                src="{{ url('/' . auth()->guard('siswa')->user()->foto) }}" alt="" width="100px" height="100px" />
                                {{-- @else --}}
                                    {{-- <img class="img-profile rounded-circle" --}}
                                        {{-- src="{{asset('assets/img/mm.jpg')}}" alt="" --}}
                                        {{-- width="100px" height="100px" /> --}}
                                
                                {{-- @endif --}}
                    </div>
                    <h5 class="mt-3 mb-1">
                    </h5>
                </div>
                {{-- <hr class="my-4"> --}}
                {{-- <div class="text-muted">
                    <div class="table-responsive mt-4">
                        @auth

                            <div>
                                <p class="mb-1">Nama : {{ auth()->guard('siswa')->user()->name }}</p>
                                <h5 class="font-size-16">
                                </h5>
                            </div>
                            <div class="mt-4">
                                <p class="mb-1">Nis : {{ auth()->guard('siswa')->user()->username }}</p>
                                <h5 class="font-size-16">
                                </h5>
                            </div>
                        @endauth
                    </div>
                </div> --}}
            </div>
        </div>
    </div>
    <div class="col-xl-8">
        <div class="card">
            <div class="card-body">
                <div class="profile-tab">
                    <div class="custom-tab-1">
                        <ul class="nav nav-tabs">
                            <li class="nav-item"><a href="#about-me" data-bs-toggle="tab"
                                    class="nav-link active show">Profil</a>
                            </li>
                        </ul>
                        <div class="tab-content">
                            <div id="about-me" class="tab-pane fade active show">
                                <div class="profile-personal-info mt-4">
                                            <h4 class="text-primary mb-4">Informasi Tambahan</h4>
                                            <form action="{{ route('update-profile') }}" method="post">
                                                @method("put")
                                                @csrf
                                            <div class="form-group">
                                                <input type="text" class="form-control form-control-user"
                                                    id="exampleInputPassword" placeholder="Nama Lengkap" value="{{old('name', Auth::guard('siswa')->user()->name)}}">
                                            </div>
                                            <div class="form-group">
                                                <input type="text" class="form-control form-control-user"
                                                    id="exampleInputPassword" placeholder="Nis" value="{{old('name', Auth::guard('siswa')->user()->username)}}">
                                            </div>
                                            {{-- <div class="form-group">
                                                <input type="file" class="form-control form-control-user"
                                                    id="exampleInputEmail" aria-describedby="emailHelp"
                                                    placeholder="Couse Foto" class="fas fa-file fa-sm text-white-50" value="{{old('name', Auth::guard('siswa')->user()->username)}}">
                                            </div> --}}
                                            <div class="form-group">
                                                <select class="form-control" name="kategori_id" id="exampleFormControlSelect1" required>
                                                    <option value="">{{old('name', Auth::guard('siswa')->user()->jenis_kelamin)}}</option>
                                                    <option value="Laki-laki">Laki-laki</option>
                                                    <option value="Perempuan">Perempuan</option>
                                                </select></div>
                                            <div class="form-group">
                                                <input type="text" class="form-control form-control-user"
                                                    id="exampleInputPassword" placeholder="Tempat Lahir" value="{{old('name', Auth::guard('siswa')->user()->tempat_lahir)}}">
                                            </div>
                                            <div class="form-group">
                                                <input type="date" class="form-control form-control-user"
                                                    id="exampleInputPassword" placeholder="Taggal Lahir" value="{{old('name', Auth::guard('siswa')->user()->taggal_lahir)}}">
                                            </div>
                                            {{-- <div class="row mb-2">
                                                <div class="col-sm-3 col-5">
                                                    <h5 class="f-w-500">Alamat
                                                    </h5>
                                                </div>
                                                <div class="col-sm-9 col-7">
                                                </div>
                                            </div> --}}
                                            <br>
                                            <h4 class="text-primary mb-4">Informasi keterangan</h4>
                                           
                                                <div class="form-group">
                                                    <input type="text" class="form-control form-control-user"
                                                        id="exampleInputPassword" placeholder="keterangan" value="{{old('name', Auth::guard('siswa')->user()->keterangan)}}">
                                                </div>
                                                <button type="submit" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i
                                                    class="fas fa-file fa-sm text-white-100"></i> Submit</button>
                                            </form>
                                </div> 
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- end row -->
@endsection 