<?php

namespace App\Models;

use App\Models\Karya;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Kategori extends Model
{
    use HasFactory;
    protected $fillable = [ 'kategori'];
    public function karya()
    {
        return $this->hasMany(Karya::class);
    }


}
