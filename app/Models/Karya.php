<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Karya extends Model
{
    use HasFactory;
    protected $fillable = ['siswa_id','file', 'kategori_id', 'deskripsi', 'kelas_id'];
    public function kategori()
    {
        return $this->belongsTo(Kategori::class);
    }
    public function kelas()
    {
        return $this->belongsTo(Kelas::class);
    }
}
