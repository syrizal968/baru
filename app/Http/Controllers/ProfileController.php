<?php

namespace App\Http\Controllers;

use App\Models\Siswa;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ProfileController extends Controller
{
    public function index()
    {
        // $siswa = Siswa::get();
        return view('profile.profile');
    }

    public function update()
    {
        return view('profile.edit-profile');
    }

    public function store(Request $request)
    {
        Auth::guard('siswa')->update([
        'name' => $request->name,
        'username' => $request->username,
        'tempat_lahir' => $request->tempat_lahir,
        'tanggal_lahir' => $request->tanggal_lahir,
        'keterangan' => $request->keterangan,
        ]);

        return view('profile.profile');
    }
}
