<?php

namespace App\Http\Controllers;

Use App\Models\User;
Use Illuminate\Support\Facades\Auth;
Use illuminate\Support\Facades\Flash;
Use illuminate\support\Facades\Session;
Use App\Http\Controllers\Controller;
Use Illuminate\Http\Request;
use App\Models\Siswa;
use Illuminate\Support\Facades\Hash;

class SessioanControler extends Controller
{
    public function index()
    {
        return view('sesi.login');
    }

    public function login(Request $request)
    {
        $request->validate([
            'username' => 'required',
            'userpasswordname' => 'required'
        ], [
            'username.required' => 'Username wajib diisi',
            'userpasswordname.required' => 'Password wajib diisi',
        ]
    );

    $password = $request->userpasswordname;
    $username = $request->username;

    $infologin =[
        'username' => $username,
        'password' => $password,
    ];
    // dd($infologin);
    // return response()->json($infologin);

    $find = Siswa::where('username', $username)->first();
    if ($find) {
        if ($password == $find->password) {
            Auth::guard('siswa')->login($find);
            return redirect()->route('dashboard',compact('find'));
        }else{
            return redirect('')->withErrors( 'Password Salah.')->withInput();
        }
    } else {
        return redirect('')->withErrors( 'Username tidak ditemukan.')->withInput();
    }

    }

    public function logout(Request $request)
    {
        // Auth::logout();
        // return redirect('/login');
        Auth()->guard('siswa')->logout();
        return redirect()->route('login');
    }

}
