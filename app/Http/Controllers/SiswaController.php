<?php

namespace App\Http\Controllers;

use App\Models\Karya;
use App\Models\Kelas;
use App\Models\Siswa;
use App\Models\Kategori;
use Illuminate\Http\Request;
use Barryvdh\DomPDF\Facade\Pdf;
use Illuminate\Support\Facades\Auth;

class SiswaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    // $karya = Auth::guard('siswa')->id;
    $kategori = Kategori::all();
    $karya = Karya::get();
    return view('dashboard', compact('karya', 'kategori'));
    }

    public function kategori($id)
    {
    $kategori = Kategori::all();
    $karya = karya::where('kategori_id');
    return view('dashboard', compact('karya'));
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $kategori = Kategori::select('id', 'kategori')->get();
        $kelas = Kelas::select('id', 'kelas')->get();

        return view('datasiswa.portofolio', ['kategori' => $kategori,'kelas' => $kelas]);
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = Karya::create($request->all());
        if ($request->hasFile('file')) {
            $request->file('file')->move('filekarya/', $request->file('file')->getClientoriginalName());
            $data->file = $request->file('file')->getClientoriginalName();
            $data->save();
        }
        return redirect()->route('dashboard');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Siswa  $siswa
     * @return \Illuminate\Http\Response
     */
    public function show(Siswa $siswa)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Siswa  $siswa
     * @return \Illuminate\Http\Response
     */
    public function edit(Siswa $siswa)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Siswa  $siswa
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Siswa $siswa)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Siswa  $siswa
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = Karya::find($id);
        $data->delete();
        return redirect()->route('dashboard');
    }

    public function cetak()
    {
        $karya = Karya::get();
        return view('pdf.cetakpdf',['karya' => $karya]);
    }
    public function export()
    {
        $siswa = Siswa::get();
        $karya = Karya::get();
        $pdf = Pdf::loadView('pdf.cetakpdf', ['karya' => $karya, 'siswa' => $siswa]);
        return $pdf->download('portofolio.pdf');
    }


}
