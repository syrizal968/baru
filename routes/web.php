<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\SiswaController;
use App\Http\Controllers\ProfileController;
use App\Http\Controllers\SessioanControler;
use SebastianBergmann\CodeCoverage\Report\Html\Dashboard;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('sesi.login');
})->name('login');

Route::get('admin/dashboard', function () {
    return view('admin.dashboard');
})->name('home');


// Route::get('/', [SessioanControler::class, 'index'])->name('login');
Route::post('login', [SessioanControler::class, 'login'])->name('loginn');

// Route::get('home', [SessioanControler::class, 'index'])->name('home')->middleware('auth');

Route::middleware(['auth:siswa'])->group(function () {
    Route::get('/profile', [ProfileController::class, 'index'])->name('profile');
    Route::get('/edit/profile', [ProfileController::class, 'update'])->name('edit-profile');
    Route::put('/update/profile', [ProfileController::class, 'store'])->name('update-profile');

    Route::get('/cetak/pdf', [SiswaController::class, 'cetak'])->name('cetak.pdf');
    Route::get('/export/pdf', [SiswaController::class, 'export'])->name('export.pdf');

    Route::get('portofolio', [SiswaController::class, 'create'])->name('portofolio');
    Route::get('dashboard', [SiswaController::class, 'index'])->name('dashboard');
    Route::post('/tambahkarya', [SiswaController::class, 'store'])->name('tambahkarya');
    Route::get('/hapuskarya/{id}', [SiswaController::class, 'destroy'])->name('hapuskarya');
    Route::get('/siswa/kategori/{id}', [SiswaController::class, 'kategori'])->name('kategory');
    Route::post('logout', [SessioanControler::class, 'logout'])->name('logout');
});








// Auth::routes(); //bawaan dari otomatis

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
