<?php

namespace Database\Seeders;

use App\Models\User;
use App\Models\Kelas;
use App\Models\Siswa;
use App\Models\Kategori;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;



class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
    Kategori::create([
        'kategori' => 'Desain Grafis'
    ]);
    Kategori::create([
        'kategori' => 'Desain Logo'
    ]);
    Kategori::create([
        'kategori' => 'Karya Tulis'
    ]);
    Kelas::create([
        'Kelas' => 'Class X'
    ]);
    Kelas::create([
        'Kelas' => 'Class XI'
    ]);
    Kelas::create([
        'Kelas' => 'Class XII'
    ]);

    User::create([
        'name' => 'Cecep',
        'email' => 'admin@gmail.com',
        'password' => '123456',
        // 'created_at' => now(),
    ]);
    Siswa::create([
        'name' => 'Asep',
        'username' => '2020',
        'password' => '123456',
        'tempat_lahir' => 'jakarta',
        'tanggal_lahir' => '12-04-2003',
        'keterangan' => 'seorang siswa SMK dengan minat dalam teknologi dan komputer. Saya aktif belajar tentang pengembangan perangkat lunak, desain web, dan jaringan komputer. Saya bersemangat untuk terus belajar dan berkontribusi dalam industri teknologi di masa depan.',
        // 'created_at' => now(),
    ]);
    }
}
